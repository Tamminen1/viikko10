/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko10;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 *
 * @author Juha
 */
public class LueXML {
    private ArrayList<Teatteri> teatterit = new ArrayList();
    private String x = "";
    private String y;
    
    public ArrayList<Teatteri> getXML() throws MalformedURLException, IOException {
        URL url = new URL("http://www.finnkino.fi/xml/TheatreAreas/");
        BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
       
        String content="";
        String line;
        
        while((line = br.readLine()) != null) {
            content += line + "\n";           
        }
        
        Parse d1 = new Parse(content);
        for(int i=0; i<19; i++) { //int i=0; i<20; i++
            y = d1.getMap().get("ID").get(i) + " " + d1.getMap().get("Name").get(i) + "\n";
            x += y;
            teatterit.add(new Teatteri(d1.getMap().get("ID").get(i),d1.getMap().get("Name").get(i)));
        }
        //System.out.println(x);
        return teatterit;
    }
}
