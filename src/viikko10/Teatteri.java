/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko10;

/**
 *
 * @author Juha
 */
public class Teatteri {
    private final String id;
    private final String name;
    
    public Teatteri(String x, String y) {
        id = x;
        name = y;
    }
    public String getId() {
        return id;
    }
    public String getName() {
        return name;
    }
}
