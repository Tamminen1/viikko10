/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko100;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import viikko10.LueXML;
import viikko10.Teatteri;
/**
 *
 * @author Juha
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Button button;
    @FXML
    private TextArea textarea;
    @FXML
    private ComboBox<String> combobox;
    private ArrayList<Teatteri> tt;
    @FXML
    private void handleButtonAction(ActionEvent event) throws IOException {
        LueXML d1 = new LueXML();
        //String temp;
        System.out.println("You clicked me!");
        //temp = d1.getXML();
        //textarea.setText(temp);
        //System.out.println("3333333");
//        tt = d1.getXML();
//        for(int i=1; i<tt.size(); i++) {
//            combobox.getItems().addAll(tt.get(i).getName());
//        }
        //choicebox.getItems().addAll("lappi","lappeenranta","helsinki");
        //ChoiceBox cb = new ChoiceBox();
        //cb.getItems().addAll("lappeenranta","kouvola","helsinki");
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        LueXML d2 = new LueXML();
        try {
            tt = d2.getXML();
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
        for(int i=1; i<tt.size(); i++) {
            combobox.getItems().addAll(tt.get(i).getName());
        }
    }    
    
}
